**ASSIGNMENT 1**

**Membri Gruppo:**

*	Emil Osterhed 807527
*	Matteo Pelucchi 806798

**GitLab Repository:**

https://gitlab.com/m.pelucchi/assigment1

**Scopo Applicazione:**

L’applicazione consiste in un sondaggio che come prima cosa richiede l’indirizzo
email. Una volta inserito viene cliccato il bottone “Inizia” con cui viene 
svolta prima una verifica della validità della email inserita e successivamente
un controllo se sia già usata precedentemente. Infatti se fosse già stata 
utilizzata verranno recuperati i vecchi dati inseriti riguardanti il sondaggio. 
Altrimenti, se non ancora usata, si viene indirizzati a una pagina in cui è 
richiesto di compilare dei campi riguardanti i modelli delle automobili.
I campi sono:
1.	Marca
2.	Modello
3.	Cilindrata
4.	Anno Immatricolazione
5.	Kilometri percorsi
6.	Prezzo di acquisto
7.	Comprata nuova o usata
Una volta compilati i campi si preme il bottone “Continua” e i dati vengono 
inseriti in un database MySQL.

![Schermata1](Document_Images/S1.png) ![Schermata2](Document_Images/S2.png)

Infine, nella schermata finale vengono mostrati una scritta di sondaggio 
completato, il risultato e la possibilità di condividerlo. Nel risultato viene
mostrato quanto le caratteristiche dell’auto si inseriscono all’interno delle 
statistiche calcolate fra tutte quelle usate nel sondaggio. In particolare, si 
mostra se l’auto inserita appartiene alla categoria delle più economiche, nella 
media o delle più costose.

![Schermata3](Document_Images/S3.png)

**Aspetti Tecnici Applicazione:**

I controlli relativi all’inserimento della email, nella schermata iniziale 
(scritta nei linguaggi Javascript e Typescript in ambiente Ionic Framework), 
sono effettuati tramite un codice php contenuto nel file “utente.php”. Tramite 
una query al database mysql si verifica che l’indirizzo email non sia già stato 
utilizzato, e in caso non sia già stata usata viene chiamata la funzione 
“inserisci.php” con la quale si aggiunge alla tabella utenti il nuovo indirizzo 
email.
Qualora invece con quell’indirizzo email si sia già risposto al sondaggio, viene 
visualizzato una finestra di dialogo che chiede se si vuole modificare le 
risposte oppure ripetere da capo il sondaggio. 

Una volta reindirizzati alla pagina del sondaggio, scritta sempre con Ionic 
Framework, si presentano una serie di campi da compilare. Con il caricamento 
della pagina, viene subito svolta una query al database per ottenere tutte le 
marche di automobile. Successivamente, in base alla marca selezionata, viene 
effettuata una query per i modelli. 
Quando si ha compilato tutti i campi, premendo il tasto “Continua” la pagina 
chiama la funzione “salva.php”, che inserisce nel database le risposte al sondaggio.

Infine nella schermata finale viene mostrato il risultato comparativo rispetto 
alle altre auto inserite nel sondaggio, per cui si calcola tramite il file 
“somma.php” la media del costo tra tutte le auto e si visualizza se la propria 
auto sia tra le più economiche, nella media o più costose.

Un aspetto da sottolineare dell’applicazione è il Responsive Design, che la 
rende perfettamente suitable anche per i dispositivi mobile, come mostrato nelle 
immagini sopra e dunque,  in grado di renderla disponibile anche come app 
Android e iOS.


**Aspetti DevOps:**

L’applicazione e il database sono caricati in tre diversi docker container, 
coprendo dunque il primo aspetto di “Containerization/Virtualization”. Il docker 
contenente la pagina Web in Ionic, all’indirizzo 
[localhost:8100](localhost:8100), usando le funzioni php nel relativo container, 
si connette al container di mysql, per effettuare le query al database tramite 
il rispettivo indirizzo ip e la porta 3306. I container per collegarsi al 
database usando il link “mysql” creato nel docker-compose che risolve 
automaticamente l’indirizzo.

Schema dei container:

![SchemaContainer](Document_Images/Schema.png)

Come secondo aspetto, che abbiamo implementato, è quello di 
“Continuos Integration/Continuos Development”. Per realizzarlo abbiamo 
utilizzato le features di GitLab messe a disposizione tramite il GitLab Runner 
e Registry. Nell’apposito file “gitlab-ci.yml” abbiamo creato una pipeline, che 
sfruttando la soluzione docker-in-docker come executor, tramite un’immagine di 
docker contenente già il docker-compose, effettua gli stage di build e test. 
Prima dell’esecuzione della build, il runner si collega al Registry della 
repository e successivamente costruisce le nuove immagini dall’ultimo codice 
sorgente appena caricato e invia queste immagini al Registry. Nella fase di test 
invece, viene utilizzato il file alternativo di “docker-compose-production.yml” 
che scarica le immagini dal Registry e avvia i container. In seguito tramite la 
funzione “utente.php” si ottengono i dati relativi agli indirizzi email degli 
utenti che hanno partecipato al sondaggio e si verifica che questi siano dei 
dati json validi. Infatti se lo fossero vorrebbe dire che il collegamento al 
database è funzionante e che i dati sono stati recuperati correttamente. Dunque 
in caso positivo il controllo restituirà nella CLI della pipeline la scritta 
“Is json” altrimenti “Is not json”. Infine vengono spenti e rimossi i container 
alla fine del test.
Questa pipeline è impostata per essere eseguita a ogni nuovo aggiornamento del 
codice sorgente.

Il terzo aspetto che abbiamo coperto è quello del “Monitoring” tramite 
l’utilizzo dell’ambiente Prometheus che permette di raccogliere i dati relativi 
al database con mysqld-exporter e analizzarli tramite l’interfaccia Grafana. Per
realizzare ciò sono stati creati altri tre container comunicanti tra loro. 

Il primo dei tre contenente Prometheus, all’indirizzo [localhost:9090](localhost:9090), 
utilizzando i collector flags offerti da mysql-exporter nel relativo container, 
si connette al container mysql, per analizzarne le relative statistiche. Inoltre 
raccoglie statische relative al container di prometheus stesso. 

L’ultimo dei tre container utilizzati, all’indirizzo [localhost:3000](localhost:3000), fornisce 
un’interfaccia grafica ai dati raccolti da prometheus e mysql-exporter. 
L’interfaccia che abbiamo creato di grafana è composta da un totale di 9 grafici 
e si concentra in particolar modo sui dati relativi al container mysql. I 
grafici sono i seguenti:

*	I primi due indicano da quanto tempo sono stati avviati i container di Prometheus e mysql.
*	“AVG operation time table” esprime, in secondi, il tempo medio di tutte le query utilizzate dall’applicazione.
*	“Totale visite tabelle” esprime il numero di visite totali di ciascuna tabella.
*	“Thread MYDB container” confronta in un grafico, avente sull’asse delle ascisse il tempo, la quantità di thread creati, in esecuzione e connessi del container mysql.
*	“Scrape duration” confronta la scrape duration dei due container: prometheus e mysql
*	“Utenti” tramite un istogramma mostra gli utenti totali iscritti al sondaggio e i nuovi utenti iscritti al sondaggio
*	“Memory usage MySql” mostra l’utilizzo della process resident memory e della virtual memory da parte del container mysql
*	“Connection error” attraverso un istogramma mostra il numero di volte che si è verificato un qualsiasi errore di connessione al database

Una parte della schermata della Dashboard di Grafana:

![Grafana](Document_Images/S4.png)

Inoltre, grazie all’uso dei volumi di Docker è stato possibile permettere la 
persistenza dei dati. Infatti tutte le risposte ai sondaggi inseriti nel 
database, anche qualora il suo container dovesse essere spento, al suo riavvio, 
queste saranno ancora disponibili. Anche le statistiche relative al monitoring 
vengono mantenute dopo un successivo riavvio dei container.




