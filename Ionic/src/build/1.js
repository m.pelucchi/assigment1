webpackJsonp([1],{

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstructionsPageModule", function() { return InstructionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__instructions__ = __webpack_require__(332);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InstructionsPageModule = /** @class */ (function () {
    function InstructionsPageModule() {
    }
    InstructionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__instructions__["a" /* InstructionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__instructions__["a" /* InstructionsPage */]),
            ],
        })
    ], InstructionsPageModule);
    return InstructionsPageModule;
}());

//# sourceMappingURL=instructions.module.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstructionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the InstructionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InstructionsPage = /** @class */ (function () {
    function InstructionsPage(navCtrl, navParams, translate, storage, alertCtrl, api, http, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.api = api;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        translate.get(["TITOLO_ISTRUZIONI",
            "ISTRUZIONI_H1",
            "ISTRUZIONI_P1",
            "ISTRUZIONI_P2",
            "ISTRUZIONI_BOTTONE",
            "ERRORE",
            "EMAILERR1",
            "ITEM1_D03",
            "ITEM2_D03",
            "EMAILERR2"]).subscribe(function (values) {
            console.log('Loaded values', values);
            _this.titolo = values.TITOLO_ISTRUZIONI;
            _this.titoloH1 = values.ISTRUZIONI_H1;
            _this.titoloH6 = values.ISTRUZIONI_H6;
            _this.titoloP1 = values.ISTRUZIONI_P1;
            _this.titoloP2 = values.ISTRUZIONI_P2;
            _this.titoloBOT = values.ISTRUZIONI_BOTTONE;
            _this.errore = values.ERRORE;
            _this.emailErr1 = values.EMAILERR1;
            _this.emailErr2 = values.EMAILERR2;
            _this.si = values.ITEM1_D03;
            _this.no = values.ITEM2_D03;
        });
    }
    Object.defineProperty(InstructionsPage.prototype, "email", {
        get: function () {
            return this._email;
        },
        set: function (v) {
            this._email = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstructionsPage.prototype, "titolo", {
        get: function () {
            return this._titolo;
        },
        set: function (v) {
            this._titolo = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstructionsPage.prototype, "titoloH1", {
        get: function () {
            return this._titoloH1;
        },
        set: function (v) {
            this._titoloH1 = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstructionsPage.prototype, "titoloH6", {
        get: function () {
            return this._titoloH6;
        },
        set: function (v) {
            this._titoloH6 = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstructionsPage.prototype, "titoloP1", {
        get: function () {
            return this._titoloP1;
        },
        set: function (v) {
            this._titoloP1 = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstructionsPage.prototype, "titoloP2", {
        get: function () {
            return this._titoloP2;
        },
        set: function (v) {
            this._titoloP2 = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InstructionsPage.prototype, "titoloBOT", {
        get: function () {
            return this._titoloBOT;
        },
        set: function (v) {
            this._titoloBOT = v;
        },
        enumerable: true,
        configurable: true
    });
    InstructionsPage.prototype.validateEmail = function () {
        var _this = this;
        if (/(.+)@(.+){2,}\.(.+){2,}/.test(this._email)) {
            this.storage.set("email", this.email).then(function () {
                _this.startSurvey();
            }).catch(function () {
                _this.alertCtrl.create({
                    title: _this.errore,
                    buttons: ['Ok']
                }).present();
            });
        }
        else if (this.email == null) {
            this.nienteMail();
        }
        else {
            this.presentAlert();
        }
    };
    InstructionsPage.prototype.nienteMail = function () {
        var alert = this.alertCtrl.create({
            title: this.errore,
            subTitle: this.emailErr1,
            buttons: ['Ok']
        });
        alert.present();
    };
    InstructionsPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: this.errore,
            subTitle: this.emailErr2,
            buttons: ['Ok']
        });
        alert.present();
    };
    InstructionsPage.prototype.presentAlert1 = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: "Hai già partecipato al sondaggio",
            subTitle: "Vuoi modificare le tue risposte?",
            buttons: [
                {
                    text: this.si,
                    handler: function () {
                        _this.navCtrl.push('Question34Page', { email: _this.email, flag: true }, {
                            animate: true,
                            direction: 'forward'
                        });
                    }
                },
                {
                    text: this.no,
                    role: 'cancel',
                    handler: function () {
                        _this.navCtrl.push('EndPage', {}, {
                            animate: true,
                            direction: 'forward'
                        });
                    }
                }
            ]
        });
        alert.present();
    };
    InstructionsPage.prototype.ngOnInit = function () {
        this.user = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["b" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].email]),
        });
    };
    InstructionsPage.prototype.load = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.http.get('http://10.0.75.1/utente.php').subscribe(function (apiresp) {
            _this.utente = [];
            apiresp.forEach(function (element) {
                _this.utente.push(element);
            });
        });
        loading.dismiss();
    };
    InstructionsPage.prototype.startSurvey = function () {
        var _this = this;
        console.log(this.valore);
        if (this.utente.some(function (e) { return e.Email === _this.email; })) {
            /* this.navCtrl.push('Question34Page', {email: this.email, flag: true}, {
              animate: true,
              direction: 'forward'
            }); */
            this.presentAlert1();
        }
        else {
            this.risposta();
            this.navCtrl.push('Question34Page', { email: this.email, flag: false }, {
                animate: true,
                direction: 'forward'
            });
        }
    };
    InstructionsPage.prototype.risposta = function () {
        var _this = this;
        console.log(this.email);
        var headers = new __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' }), options = { "Email": this.email }, url = "http://10.0.75.1/inserisci.php";
        this.http
            .post(url, JSON.stringify(options), headers)
            .subscribe(function (data) {
            console.log(data);
            // If the request was successful notify the user
            console.log("Congratulations the technology: " + _this.email + " was successfully updated");
        }, function (error) {
            console.log('Errore!');
        });
    };
    InstructionsPage.prototype.ionViewDidLoad = function () {
        this.load();
    };
    InstructionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-instructions',template:/*ion-inline-start:"C:\Users\mpelu\Documents\Lavoro\Progetto\src\pages\instructions\instructions.html"*/'<!--\n  Generated template for the InstructionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar hideBackButton>\n    <ion-title>{{titolo}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <div class="splash-bg">\n      \n                  \n    <ion-img height="100%" width="100%" src="../assets/img/leasy_logo.png"></ion-img>\n                  \n              \n  </div>\n\n  <div padding>\n    \n    <form novalidate [formGroup]="user">\n  \n      <h1 class="titolo-H1" style="font-family: Ubuntu">{{titoloH1}}</h1>\n      <h6 class="titolo-H6">{{titoloH6}}</h6>\n  \n      <ion-item padding>\n      <ion-label >Email</ion-label>\n      <ion-input type="text" value="" formControlName="email" [(ngModel)]="email"></ion-input>\n      </ion-item>\n      \n      <button ion-button icon-end large clear (click)="validateEmail()" block>\n        {{titoloBOT}}\n        <ion-icon name="arrow-forward"></ion-icon>\n      </button>\n    \n    </form>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\mpelu\Documents\Lavoro\Progetto\src\pages\instructions\instructions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__providers__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], InstructionsPage);
    return InstructionsPage;
}());

//# sourceMappingURL=instructions.js.map

/***/ })

});
//# sourceMappingURL=1.js.map