webpackJsonp([0],{

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question34PageModule", function() { return Question34PageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__question34__ = __webpack_require__(333);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var Question34PageModule = /** @class */ (function () {
    function Question34PageModule() {
    }
    Question34PageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__question34__["a" /* Question34Page */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__question34__["a" /* Question34Page */]),
            ],
        })
    ], Question34PageModule);
    return Question34PageModule;
}());

//# sourceMappingURL=question34.module.js.map

/***/ }),

/***/ 333:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Question34Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(118);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the Question34Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Question34Page = /** @class */ (function () {
    function Question34Page(navCtrl, navParams, translate, _counter, alertCtrl, currencyPipe, loadingCtrl, api, http) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this._counter = _counter;
        this.alertCtrl = alertCtrl;
        this.currencyPipe = currencyPipe;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.http = http;
        this.items = [];
        translate.get(["TITOLO_DOMANDA",
            "DOMANDA_35",
            "ERRORE",
            "SUB",
            "TUTORIAL_CONTINUE_BUTTON",
            "AVVISO",
            "IMPORTO",
            "RATE",
            "ITEM1_D35",
            "ITEM2_D35",
            "ITEM3_D35",
            "ITEM4_D35",
            "ITEM5_D35",
            "ITEM6_D35",
            "ITEM7_D35",
            "ITEM8_D35",
            "ITEM9_D35",
            "VALUTA",
            "VALUTA1",
            "NUOVA",
            "USATA"
        ]).subscribe(function (values) {
            _this.counter.get("counter").then(function (valore) {
                _this.currentCounter = valore;
                _this.titolo = values.TITOLO_DOMANDA;
            });
            _this.domanda = values.DOMANDA_35;
            _this.errore = values.ERRORE;
            _this.titoloBOT = values.TUTORIAL_CONTINUE_BUTTON;
            _this.avviso = values.AVVISO;
            _this.importo = values.IMPORTO;
            _this.sub = values.SUB;
            _this.rateScritta = values.RATE;
            _this.marcaTesto = values.ITEM1_D35;
            _this.valutaScritta = values.VALUTA;
            _this.valutaScritta1 = values.VALUTA1;
            _this.comprato = values.ITEM6_D35;
            _this.selectOptions = {
                title: _this.valutaScritta1
            };
            _this.selectOptions = {
                title: _this.marcaTesto
            };
            _this.modelloTesto = values.ITEM2_D35;
            _this.selectOptions1 = {
                title: _this.modelloTesto
            };
            _this.dataTesto = values.ITEM3_D35;
            _this.hpTesto = values.ITEM4_D35;
            _this.kmTesto = values.ITEM5_D35;
            _this.Nuova = values.NUOVA;
            _this.NuovaoUsata = values.ITEM7_D35;
            _this.Usata = values.USATA;
            _this.email = navParams.get('email');
            _this.flag = navParams.get('flag');
            _this.selectOptions2 = {
                title: _this.NuovaoUsata
            };
        });
    }
    Object.defineProperty(Question34Page.prototype, "marche", {
        get: function () {
            return this._marche;
        },
        set: function (v) {
            this._marche = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Question34Page.prototype, "marca", {
        get: function () {
            return this._marca;
        },
        set: function (v) {
            this._marca = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Question34Page.prototype, "rate", {
        get: function () {
            return this._rate;
        },
        set: function (v) {
            this._rate = v;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Question34Page.prototype, "counter", {
        get: function () {
            return this._counter;
        },
        set: function (value) {
            this._counter = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Question34Page.prototype, "titolo", {
        get: function () {
            return this._titolo;
        },
        set: function (v) {
            this._titolo = v;
        },
        enumerable: true,
        configurable: true
    });
    Question34Page.prototype.test = function (id) {
        console.log(id);
        console.log(this.checkbox);
    };
    Question34Page.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: this.errore,
            subTitle: this.sub,
            buttons: ['Ok']
        });
        alert.present();
    };
    Question34Page.prototype.presentAlert1 = function () {
        var alert = this.alertCtrl.create({
            title: this.errore,
            subTitle: this.valutaScritta,
            buttons: ['Ok']
        });
        alert.present();
    };
    Question34Page.prototype.onChangePrice = function (evt) {
        this.box_price = evt.replace(/[^0-9.]/g, "");
        if (this.box_price) {
            this.box_price_formatted = this.getCurrency(this.box_price);
            console.log("box_price_formatted: " + this.box_price_formatted);
        }
    };
    Question34Page.prototype.onPriceUp = function (evt) {
        this.box_price = evt.replace(/[^0-9.]/g, "");
        this.box_price_formatted = String(this.box_price);
    };
    Question34Page.prototype.getCurrency = function (amount) {
        return this.currencyPipe.transform(amount, this.valuta, 'symbol', '1.2-2');
    };
    Question34Page.prototype.Controllo = function () {
        if (this.valuta == null) {
            this.presentAlert1();
        }
    };
    Question34Page.prototype.Next = function () {
        var _this = this;
        if (this.marca != null && this.modello != null && this.data != null && this.hp != null && this.km != null && this.valuta != null && this.box_price_formatted != null && this.nuovo != null) {
            var loading_1 = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading_1.present();
            //this.salva();
            if (this.flag == true) {
                this.api.post("/update.php", {
                    Email: this.email,
                    Marca: this.marca,
                    Modello: this.modello,
                    Anno: this.data,
                    HP: this.hp,
                    Km: this.km,
                    Nuova: this.nuovo,
                    Prezzo: this.box_price,
                    Valuta: this.valuta,
                }).subscribe(function (apiresponse) {
                    console.log(apiresponse);
                    loading_1.dismiss();
                    _this.navCtrl.push('EndPage', { prezzo: _this.box_price }, {
                        animate: true,
                        direction: 'forward'
                    });
                });
            }
            else {
                this.api.post("/salva.php", {
                    Email: this.email,
                    Marca: this.marca,
                    Modello: this.modello,
                    Anno: this.data,
                    HP: this.hp,
                    Km: this.km,
                    Nuova: this.nuovo,
                    Prezzo: this.box_price,
                    Valuta: this.valuta,
                }).subscribe(function (apiresponse) {
                    console.log(apiresponse);
                    loading_1.dismiss();
                    _this.navCtrl.push('EndPage', { prezzo: _this.box_price }, {
                        animate: true,
                        direction: 'forward'
                    });
                });
            }
        }
        else {
            this.presentAlert();
        }
    };
    /* salva()
     {
       console.log(this.email);
        let headers 	: any		= new HttpHeaders({ 'Content-Type': 'application/json' }),
            options 	: any		= { "Email" : this.email, "Marca": this.marca, "Modello": this.modello, "Anno":this.anno, "HP":this.hp, "Km":this.km, "Nuova":this.nuovo, "Prezzo":this.box_price_formatted, "Valuta":this.valuta},
            url       : any   = "http://10.0.75.1/salva.php";
  
        this.http
        .post(url, JSON.stringify(options), headers)
        .subscribe(data =>
        {
          console.log(data);
           // If the request was successful notify the user
           console.log(`Congratulations the technology: ${this.email} was successfully updated`);
        },
        (error : any) =>
        {
           console.log('Errore!');
        });
     } */
    Question34Page.prototype.ordina = function (a, b) {
        if (a.Description > b.Description) {
            return 1;
        }
        else {
            return -1;
        }
    };
    Question34Page.prototype.marcaSelezionata = function (event) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.api.post("/modelli.php", {
            Description: event
        }).subscribe(function (apiresp) {
            _this.modelli = [];
            apiresp.forEach(function (element) {
                _this.modelli.push(element);
            });
            loading.dismiss();
        });
    };
    Question34Page.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (this.email == null) {
            this.navCtrl.push('InstructionsPage', {}, {
                animate: true,
                direction: 'forward'
            });
        }
        else {
            var loading = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            this.load();
            if (this.flag == true) {
                this.api.post("/risposte.php", {
                    Email: this.email
                }).subscribe(function (apiresp) {
                    _this.marca = apiresp[0].Marca;
                    _this.marcaSelezionata(_this.marca);
                    _this.modello = apiresp[0].Modello;
                    _this.data = apiresp[0].Anno;
                    _this.hp = apiresp[0].HP;
                    _this.km = apiresp[0].Km;
                    //this.box_price = apiresp[0].Prezzo;
                    _this.nuovo = apiresp[0].Nuova;
                    _this.valuta = apiresp[0].Valuta;
                    _this.onChangePrice(apiresp[0].Prezzo);
                });
                loading.dismiss();
            }
            else {
                loading.dismiss();
            }
        }
    };
    Question34Page.prototype.load = function () {
        var _this = this;
        this.http.get('http://10.0.75.1/marche.php').subscribe(function (apiresp) {
            _this.marche = [];
            apiresp.forEach(function (element) {
                _this.marche.push(element);
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Navbar */])
    ], Question34Page.prototype, "navBar", void 0);
    Question34Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-question34',template:/*ion-inline-start:"C:\Users\mpelu\Documents\Lavoro\Progetto\src\pages\question34\question34.html"*/'<!--\n  Generated template for the Question34Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar hideBackButton>\n    <ion-title>{{titolo}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <h5>{{domanda}}</h5>\n  <h6>{{marcaTesto}}:</h6>\n  <ion-item>\n      <ion-label >{{marcaTesto}}</ion-label>\n      <ion-select [(ngModel)]="marca" [selectOptions]="selectOptions" (ionChange)="marcaSelezionata($event)">\n        <ion-option *ngFor="let marcax of marche" [value]="marcax.Description">{{marcax.Description}}</ion-option>\n      </ion-select>\n  </ion-item>\n  <h6>{{modelloTesto}}:</h6>\n  <ion-item>\n      <ion-label >{{modelloTesto}}</ion-label>\n      <ion-select [(ngModel)]="modello" [selectOptions]="selectOptions1">\n        <ion-option *ngFor="let modellox of modelli" [value]="modellox.Description">{{modellox.Description}}</ion-option>\n      </ion-select>\n  </ion-item>\n  <h6>{{dataTesto}}:</h6>\n  <ion-item>\n      <ion-label>{{dataTesto}}</ion-label>\n      <ion-datetime displayFormat="YYYY" min="1980" [(ngModel)]="data"></ion-datetime>\n    </ion-item>\n    <h6>{{hpTesto}}:</h6>\n    <ion-item>\n        <ion-label>{{hpTesto}}</ion-label>\n        <ion-input type="number" [(ngModel)]="hp"></ion-input>\n    </ion-item>\n    <h6>{{kmTesto}}:</h6>\n    <ion-item>\n        <ion-label>Km</ion-label>\n        <ion-input type="number" [(ngModel)]="km"></ion-input>\n    </ion-item>\n    <h6>{{comprato}}:</h6>\n    <ion-grid>\n        <ion-row>\n            <ion-col>\n                <ion-item>\n                  <ion-input placeholder="{{importo}}" type="string"  [(ngModel)]="box_price_formatted" (change)=\'onChangePrice($event.target.value)\' (keyup)="onPriceUp($event.target.value)" (click)=\'Controllo()\'></ion-input>\n                </ion-item>\n            </ion-col>\n            <ion-col>\n                <ion-select [(ngModel)]="valuta" placeholder="{{valutaScritta1}}" [selectOptions]="selectOptions" style="width: 100%; max-width: 100%;">\n                    <ion-option value="CHF">CHF</ion-option>\n                    <ion-option value="EUR">EUR(€)</ion-option>\n                    <ion-option value="USD">USD($)</ion-option>\n                    <ion-option value="GBP">GBP(£)</ion-option>\n                  </ion-select>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <h6>{{NuovaoUsata}}:</h6>\n        <ion-item>\n            <ion-select [(ngModel)]="nuovo" [selectOptions]="selectOptions2" style="width: 100%; max-width: 100%;">\n                <ion-option value="NuovaVal">{{Nuova}}</ion-option>\n                <ion-option value="UsataVal">{{Usata}}</ion-option>\n            </ion-select>\n        </ion-item>\n  </ion-content>\n  <ion-footer>\n    <button ion-button icon-end large clear (click)="Next()" block>\n      {{titoloBOT}}\n      <ion-icon name="arrow-forward"></ion-icon>\n    </button>\n  </ion-footer>'/*ion-inline-end:"C:\Users\mpelu\Documents\Lavoro\Progetto\src\pages\question34\question34.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_4__angular_common__["c" /* CurrencyPipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_5__providers__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */]])
    ], Question34Page);
    return Question34Page;
}());

//# sourceMappingURL=question34.js.map

/***/ })

});
//# sourceMappingURL=0.js.map