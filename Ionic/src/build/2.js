webpackJsonp([2],{

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndPageModule", function() { return EndPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__end__ = __webpack_require__(331);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EndPageModule = /** @class */ (function () {
    function EndPageModule() {
    }
    EndPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__end__["a" /* EndPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__end__["a" /* EndPage */]),
            ],
        })
    ], EndPageModule);
    return EndPageModule;
}());

//# sourceMappingURL=end.module.js.map

/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EndPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__(29);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EndPage = /** @class */ (function () {
    function EndPage(navCtrl, navParams, translate, alertCtrl, currencyPipe, loadingCtrl, api, http, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.currencyPipe = currencyPipe;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.http = http;
        this.platform = platform;
        this.showSkip = false;
        this.dir = platform.dir();
        translate.get([
            "GRAZIE",
            "TUTORIAL_SLIDE1_TITLE",
            "TUTORIAL_SLIDE1_DESCRIPTION",
            "TUTORIAL_SLIDE2_TITLE",
            "TUTORIAL_SLIDE2_DESCRIPTION",
            "TUTORIAL_SLIDE2_DESCRIPTION1",
            "TUTORIAL_SLIDE2_DESCRIPTION2",
            "TUTORIAL_SLIDE3_TITLE",
            "TUTORIAL_SLIDE3_DESCRIPTION"
        ]).subscribe(function (values) {
            _this.grazie = values.GRAZIE;
            _this.titolo = values.TUTORIAL_SLIDE3_TITLE;
            _this.description = values.TUTORIAL_SLIDE3_DESCRIPTION;
            _this.titolo1 = values.TUTORIAL_SLIDE1_TITLE;
            _this.description1 = values.TUTORIAL_SLIDE1_DESCRIPTION;
            _this.titolo2 = values.TUTORIAL_SLIDE2_TITLE;
            _this.bool = 1;
            _this.description2 = values.TUTORIAL_SLIDE2_DESCRIPTION;
            _this.description21 = values.TUTORIAL_SLIDE2_DESCRIPTION1;
            _this.description22 = values.TUTORIAL_SLIDE2_DESCRIPTION2;
            _this.prezzo = navParams.get('prezzo');
        });
    }
    EndPage.prototype.onSlideChangeStart = function (slider) {
        this.slider = slider;
        this.showSkip = !slider.isEnd();
        //slider.startAutoplay(7000);
    };
    EndPage.prototype.stop = function () {
        if (this.slides._autoplaying == true) {
            this.slides.stopAutoplay();
        }
    };
    EndPage.prototype.confronta = function (dev, media) {
        if (this.prezzo < (media - dev)) {
            this.url = 'assets/img/Monopattino.png';
            this.testo = this.description22;
        }
        else if ((this.prezzo > (media - dev)) && (this.prezzo < (media + dev))) {
            this.url = "assets/img/Camion.png";
            this.testo = this.description21;    
        }
        else {
            this.url = "assets/img/Suv.png";
            this.testo = this.description2;            
        }       
    }
    EndPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.api.post("/somma.php", {}).subscribe(function (apiresp) {
            _this.confronta(apiresp[0].Dev, apiresp[0].Media);
        });
        loading.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('slides', { read: __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Slides */] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Slides */])
    ], EndPage.prototype, "slides", void 0);
    EndPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-end',template:/*ion-inline-start:"C:\Users\mpelu\Documents\Lavoro\Progetto\src\pages\end\end.html"*/' <ion-header no-shadow>\n    <!-- <ion-title>{{grazie}}</ion-title> -->\n  </ion-header>\n  \n  \n  <ion-content no-bounce>\n    \n\n\n    <ion-slides #slides pager="true" dir="{{dir}}" (ionSlideWillChange)="onSlideChangeStart($event)" autoplay="5000" loop="true">\n        <ion-slide padding speed="500">\n            <img src=\'assets/img/Pollice.png\' class="slide-image" />\n            <h2 class="slide-title">{{titolo1}}</h2>\n            <p>{{description1}}</p>\n        </ion-slide>\n        <ion-slide padding speed="700">\n            <img src={{url}} class="slide-image" />\n            <h2 class="slide-title">{{titolo2}}</h2>\n            <p>{{testo}}</p>\n          </ion-slide>\n        <ion-slide padding speed="1000">\n          <div> \n            <ion-fab center>\n              <button ion-fab (click)="stop()">\n                <ion-icon name="md-share" role="img" class="icon icon-ios ion-md-share" aria-label="share"></ion-icon>\n              </button>\n              <ion-fab-list side="down">\n                <button ion-fab color="primary">\n                    <ion-icon name="logo-facebook" role="img" class="icon icon-ios ion-logo-facebook" aria-label="logo facebook"></ion-icon>\n                </button>\n                <button ion-fab color="dark"> \n                    <ion-icon name="logo-twitter" role="img" class="icon icon-ios ion-logo-twitter" aria-label="logo twitter"></ion-icon>\n                </button>\n                <button ion-fab color="secondary"> \n                    <ion-icon name="logo-whatsapp" role="img" class="icon icon-ios ion-logo-whatsapp" aria-label="logo whatsapp"></ion-icon>\n                </button>\n                <button ion-fab color="black"> \n                    <ion-icon name="logo-instagram" role="img" class="icon icon-ios ion-logo-instagram" aria-label="logo instagram"></ion-icon>\n                </button>\n              </ion-fab-list>\n            </ion-fab>\n          <img src=\'assets/img/Cartello.png\' class="slide-image" />\n          </div>\n          <h2 class="slide-title">{{titolo}}</h2>\n        </ion-slide>\n    </ion-slides>\n    \n  </ion-content>\n'/*ion-inline-end:"C:\Users\mpelu\Documents\Lavoro\Progetto\src\pages\end\end.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__angular_common__["c" /* CurrencyPipe */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers__["a" /* Api */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */]])
    ], EndPage);
    return EndPage;
}());

//# sourceMappingURL=end.js.map

/***/ })

});
//# sourceMappingURL=2.js.map