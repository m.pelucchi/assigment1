<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

include 'db-config.php';

$conn = new mysqli($servername, $username, $password, $dbname);
$query = "SELECT Description FROM CarMakes";
$dbresult = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($dbresult)) {
            $rows[] = $r;
}
print json_encode($rows);
?>