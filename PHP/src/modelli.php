<?php
header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

include 'db-config.php';

$conn = new mysqli($servername, $username, $password, $dbname);

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$echo = $request->echo;
$marca= filter_var($request->Description);

$query = "select
CarModel.Description
from CarModel
left join MakeModels
on CarModel.ModelId = MakeModels.ModelId
left join CarMakes
on MakeModels.ModelId = CarMakes.MakeId
where CarModel.ModelId in
(select MakeModels.ModelID
from MakeModels
where
MakeModels.MakeId in
(select  CarMakes.MakeId
from CarMakes where CarMakes.Description= '$marca'))";

$dbresult = $conn->query($query);
$rows = array();
while($r = mysqli_fetch_assoc($dbresult)) {
            $rows[] = $r;
}
print json_encode($rows);
?>